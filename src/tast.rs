extern crate symbol;
use symbol::Symbol;
extern crate polytype;
use self::polytype::*;
use nodetype::Cont;
use ast::NodeBox;

pub type TNodeCont = Cont<TNodeBox>;

#[derive(Clone, Debug)]
pub struct TNode {
  pub cont: TNodeCont,
  pub pos: (usize, usize, usize),
  pub ty: Type,
}

pub type TNodeBox = Box<TNode>;

impl Default for TNode {
  fn default() -> TNode { TNode{ cont: Cont::Unit, pos: (0 , 0, 0), ty: tp!(unit)} }
}
impl TNode {
  pub fn set_pos(&mut self, b: usize, e:usize, fid:usize) { self.pos = (fid, b, e); }
  pub fn set_cont(&mut self, c:TNodeCont) { self.cont = c; }
  pub fn set_ty(&mut self, ty: Type) { self.ty = ty; }
  pub fn new_box(cont: TNodeCont, pos: (usize, usize, usize), ty: Type) -> TNodeBox {
    box TNode {cont, pos, ty}
  }
}
