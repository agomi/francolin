#![allow(unused)] // for linter
#![feature(box_syntax, box_patterns)]
#![feature(nll)]

extern crate symbol;
extern crate polytype;

use std::fmt;
use std::cell::RefCell;
use std::rc::Rc;
use std::collections::HashMap;
use std::collections::HashSet;
use symbol::Symbol;
use super::{
    ast::*,
    tast::*,
    nodetype::Cont,
};
use self::polytype::*;

#[derive(Debug, Clone)]
pub enum EnvType {
    TySch(TypeSchema),
    Ty(Type),
}

pub struct Env(Vec<HashMap<Symbol, EnvType>>);

impl Env {
    fn new()->Self {
        Env(vec![HashMap::new()])
    }

    fn leave_scope(&mut self) {
        if self.0.is_empty() {
            panic!("can't leave from the static scope!")
        }
        self.0.pop();
    }
    fn enter_scope(&mut self) {
        self.0.push(HashMap::new());
    }

    fn insert(&mut self, sym: Symbol, t: EnvType) {
        if let Some(last) = self.0.last_mut() {
            last.insert(sym, t);
        }
    }

    fn insert_ty(&mut self, sym: Symbol, ty: Type) {
        self.insert(sym, EnvType::Ty(ty))
    }

    fn insert_tysch(&mut self, sym: Symbol, tysch: TypeSchema) {
        self.insert(sym, EnvType::TySch(tysch))
    }

    fn get_instance(&self, sym: Symbol, ctx: &mut Context)->Option<Type> {
        for hm in self.0.iter().rev() {
            if let Some(ty) = hm.get(&sym) {
                return match ty {
                    EnvType::Ty(ty) => Some(ty.clone()),
                    EnvType::TySch(tysch) => Some(tysch.instantiate(ctx)),
                }
            }
        }
        None
    }
}

//pub type Env = HashMap<Symbol, TypeSchema>;

pub struct Tinfer;
impl Tinfer {
    fn teq(actual: Type, expect: Type)->bool {
        actual == expect
    }
    fn unify(&mut self, ast: NodeBox, ctx: &mut Context, env: &mut Env)->Result<TNodeBox, String> {
        let box Node{cont, pos} = ast;
        match cont {
            Cont::Unit => Ok(TNode::new_box(Cont::Unit, pos, tp!(unit))),
            Cont::IntLit(i) => Ok(TNode::new_box(Cont::IntLit(i), pos, tp!(int))),
            Cont::Sym(x) => {
                let ty = env.get_instance(x, ctx).ok_or(format!("symbol {} not found", x))?;
                Ok(TNode::new_box(Cont::Sym(x), pos, ty))
            },
            Cont::Let{name, ty: expect_ty, val} => {
                let tval = self.unify(val, ctx, env)?;
                let box tval = tval;
                if let Some(ref et) = expect_ty {
                    if !Self::teq(tval.ty.clone(), tp!(et)) {return Err(format!("type mismatch. expected: {}, actual: {}", et, tval.ty.clone()))}
                }
                env.insert_ty(name, tval.ty.clone());
                Ok(TNode::new_box(Cont::Let{name, ty: expect_ty, val: box tval}, pos, tval.ty.clone()))
            },
            Cont::Call(f, v) => {
                let mut argt: Vec<Type> = vec![];
                let mut argn: Vec<TNodeBox> = vec![];
                for a in v {
                    let box arg = self.unify(a, ctx, env)?;
                    //t.apply_mut(ctx);
                    argt.push(arg.ty.clone());
                    argn.push(box arg);
                }
                argt.push(ctx.new_variable());
                let mut fn_ty = Type::from(argt);
                let mut f_ty = self.unify(f, ctx, env)?;
                ctx.unify(&fn_ty, &f_ty.ty).map_err(|x|{"unification error"})?;
                //fn_ty.apply_mut(ctx);
                //f_ty.1.apply_mut(ctx);
                let ret_ty = fn_ty.returns().ok_or("no return type")?;
                Ok(TNode::new_box(Cont::Call(f_ty, argn), pos, ret_ty.clone()))
            },
            Cont::Fun{ name, arg: ps, retTy, body:  b} => {
                env.enter_scope();
                //ps.iter().for_each(|p| env.insert_ty(*p, ctx.new_variable()));
                
                let mut ps_ty = ps.into_iter().map(|p| {
                    let newv = ctx.new_variable();
                    env.insert_ty(p.0, newv.clone());
                    (p.0, newv)
                }).collect::<Vec<_>>();

                let mut ret_ty = self.unify(b, ctx, env).expect("result type unify");
                let ps_ty = ps_ty.into_iter().map(|p| {
                    let p = p;
                    ctx.unify(&p.1, &ret_ty).expect("param unify");
                    //ret_ty.1.apply_mut(&ctx);
                    //p.1.apply_mut(&ctx);
                    p
                }).collect::<Vec<_>>();
                let mut ty_vec = ps_ty.iter().map(|p|p.1.clone()).collect::<Vec<_>>();
                ty_vec.push(ret_ty);
                let lam_ty = Type::from(ty_vec);
                env.leave_scope();
                box (TNodeCont::Fun(ps_ty, ret_ty), lam_ty)
            },
            Cont::Statements(exps) => {
                let exps_ty = exps.into_iter().map(|exp|{
                    self.unify(exp, ctx, env)
                }).collect::<Vec<_>>();
                let seq_ty = if let Some(l) = exps_ty.last() {
                    l.1.clone()
                } else {
                    tp![unit]
                };
                box (TNodeCont::Seq(exps_ty), seq_ty)
            },
        }
    }

    fn apply(&mut self, ast: &mut Box<(TNodeCont, Type)>, ctx: &mut Context)/*->Box<(TNodeCont, Type)>*/ {
        let box ast = ast;
        match ast {
            (TNodeCont::App(f, ps), ty) => {
                ps.iter_mut().for_each(|p|{
                    p.1.apply_mut(ctx);
                    self.apply(p, ctx);
                });
                f.1.apply_mut(ctx);
                ty.apply_mut(ctx);
                self.apply(f, ctx);
            },
            (TNodeCont::Lam(ps, b), ty) => {
                ps.iter_mut().for_each(|p|{
                    p.1.apply_mut(ctx);
                });
                b.1.apply_mut(ctx);
                ty.apply_mut(ctx);
                self.apply(b, ctx);
            },
            (TNodeCont::Let(_, t), ty) => {
                t.1.apply_mut(ctx);
                ty.apply_mut(ctx);
                self.apply(t, ctx);
            },
            (TNodeCont::Seq(v), ty) => {
                v.iter_mut().for_each(|e| {
                    e.1.apply_mut(ctx);
                    self.apply(e, ctx);
                });
                ty.apply_mut(ctx);
            },
            (TNodeCont::Sym(_, t), ty) => {
                t.apply_mut(ctx);
                ty.apply_mut(ctx);
            },
            _ => ()
        }
    }

    fn tinf(&mut self, ast: Box<NodeCont>, ctx: &mut Context, env: &mut Env)->Box<(TNodeCont, Type)> {
        let mut ast = self.unify(ast, ctx, env);
        ctx.reduct_substitution();
        self.apply(&mut ast, ctx);
        ast.1.apply_mut(ctx);
        ast
    }
}

mod dsl {
    use super::Exp;
    pub fn v(x: &str)->Box<Exp>{box Exp::Sym(x.into())}
    pub fn let_(x: &str, e1: Box<Exp>, e2: Box<Exp>)->Box<Exp>{box Exp::Let(x.into(), e1, e2)}
    pub fn app(e1: Box<Exp>, e2: Box<Exp>)->Box<Exp>{box Exp::App(e1, e2)}
    pub fn int(i: isize)->Box<Exp>{box Exp::IntLit(i)}
    pub fn lam(x: &str, e: Box<Exp>)->Box<Exp> {box Exp::Lam(x.into(), e)}
}
