extern crate symbol;
use symbol::Symbol;

pub type TypeName = Symbol;

#[derive(Clone, Debug, PartialEq)]
pub enum Cont<T> {
  Unit,
  BoolLit(bool),
  IntLit(i64),
  FloatLit(f64),
  StrLit(String),
  Sym(Symbol),
  Call(T, Vec<T>),
  Statements(Vec<T>),
  Let {name: Symbol, ty: Option<TypeName>, val: T},
  Fun {name: Symbol, arg: Vec<(Symbol, TypeName)>, retTy: TypeName, body: T},
  If (Vec<(T, T)>),
  While {cond: T, body: T, },

}