extern crate symbol;
use symbol::Symbol;
use std::cell::RefCell;
use nodetype::Cont;


pub type NodeCont = Cont<NodeBox>;

#[derive(Clone, Debug)]
pub struct Node {
  pub cont: NodeCont,
  pub pos: (usize, usize, usize),
}

pub type NodeBox = Box<Node>;

impl Default for Node {
  fn default() -> Node { Node{ cont: Cont::Unit, pos: (0 , 0, 0),} }
}
impl Node {
  fn set_pos(&mut self, b: usize, e:usize, fid:usize) { self.pos = (fid, b, e); }
  fn set_cont(&mut self, c:NodeCont) { self.cont = c; }
}
