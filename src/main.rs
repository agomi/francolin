#![allow(unused)] // for linter
#![feature(box_syntax, box_patterns)]
#![feature(plugin)]

extern crate symbol;

mod nodetype;
mod ast;
mod tinf;
mod tast;

use std::boxed::Box;
use std::cell::RefCell;
use std::collections::hash_map::HashMap;
use symbol::Symbol;
use ast::{Node, NodeBox, NodeCont};


#[macro_export]
macro_rules! node {
  ($token:ident $args:tt, $b:expr , $e:expr ) => ( box Node{cont: NodeCont::$token$args, pos: (0, $b, $e)});
  ($token:ident $args:tt ) => ( box Node{cont: NodeCont::$token$args, pos: (0, 0, 0)});
  ($token:ident , $b:expr , $e:expr ) => ( box Node{cont: NodeCont::$token, pos: (0, $b, $e)});
  ($token:ident) => ( box Node{cont: NodeCont::$token, pos: (0, 0, 0)});
} 

mod parser {
  include!(concat!(env!("OUT_DIR"), "/parser.rs"));
}
fn write_class(n: NodeBox, file: String) {
    unimplemented!()
}

fn interpret(node: NodeBox)->NodeCont {
  let pos = (*node).pos;
  let n = (*node).cont;
  match n {
      NodeCont::IntLit(i)=>n,
      NodeCont::FloatLit(f)=>n,
      //NodeCont::
      _=>unimplemented!()
  }
  
}

fn main() {
	println!("{:#?}", parser::parse("if 1 {2} elsif 2{ 3+1; } else {3}", 0));
  
}
